package Lecture7.Task1.Interfaces;


import Lecture7.Task1.Finance;

public interface Exchange {

    public double convert(int uah, String curr);
}
