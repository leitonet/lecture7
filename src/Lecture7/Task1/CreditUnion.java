package Lecture7.Task1;


import Lecture7.Task1.Interfaces.Kredit;

public class CreditUnion extends Finance implements Kredit {

    private double percentOfKredit;

    public CreditUnion(String name, String adress, double percentOfKredit) {
        super(name, adress);
        this.percentOfKredit = percentOfKredit;
    }

    @Override
    public String getInfo() {
        return super.getInfo();
    }

    @Override
    public int giveKredit(int uah) {
        if (uah > 100_000) {
            throw new IllegalArgumentException("Must be less then 100_000 grn!");
        }else {
            return uah;
        }
    }
}
