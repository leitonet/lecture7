package Lecture7.Task1;

import Lecture7.Task1.Interfaces.Exchange;
import Lecture7.Task1.Interfaces.Investment;
import Lecture7.Task1.Interfaces.Kredit;
import Lecture7.Task1.Interfaces.Sender;

public class Bank extends Finance implements Exchange, Kredit, Investment, Sender {

    private long uah;
    private double percentOfKredit;
    private double percentOfSending;
    private Currency currency;

    public Bank(String name, String address, double usd, double eur, long uah, double percentOfKredit, double percentOfSending) {
        super(name, address);
        this.uah = uah;
        this.percentOfKredit = percentOfKredit;
        this.percentOfSending = percentOfSending;
        currency = new Currency(usd, eur);
    }

    @Override
    public String getInfo() {
        return super.getInfo();
    }

    @Override
    public double convert(int uah, String curr) {
        if (uah > 12000) {
            throw new IllegalArgumentException("Must be less then 12000 uan");
        } else {
            if(curr.equals("eur")) {
                return (uah - 15) / currency.eur;
            }
            if(curr.equals("usd")){
                return (uah - 15) / currency.usd;
            }
        }
        return 0;
    }


    @Override
    public int giveKredit(int uah) {
        if (uah > 200_000) {
            throw new IllegalArgumentException("Must be less then 200_000 grn!");
        } else {
            return uah;
        }
    }

    @Override
    public void giveMoneyOnInvestment(int uah) {
        this.uah += uah;
    }

    @Override
    public double send(double uah) {
        return uah + (uah * this.percentOfSending) - 5;
    }
}