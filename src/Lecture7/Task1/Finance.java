package Lecture7.Task1;


public abstract class Finance implements Comparable<Finance>{
    public String name;
    public String address;


    public Finance(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getInfo(){
        return this.getClass().getSimpleName() + " *** Name: " + this.name + ". *** Address: " + this.address;
    }

    @Override
    public int compareTo(Finance o) {
        return name.compareTo(o.name);
    }
}
