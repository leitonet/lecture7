package Lecture7.Task1;


import Lecture7.Task1.Interfaces.Sender;

public class Post extends Finance implements Sender{

    private double percentOfSend;

    public Post(String name, String adress, double percentOfSend) {
        super(name, adress);
        this.percentOfSend = percentOfSend;
    }

    @Override
    public String getInfo() {
        return super.getInfo();
    }

    @Override
    public double send(double uah) {
        return uah + (uah * percentOfSend);
    }
}
