package Lecture7.Task1;


import Lecture7.Task1.Interfaces.Investment;

public class MIF extends Finance implements Investment {

    private int uah;
    int year;

    public MIF(String name, String adress, int year) {
        super(name, adress);
        this.year = year;
    }

    @Override
    public String getInfo() {
        return super.getInfo();
    }

    @Override
    public void giveMoneyOnInvestment(int uah) {
        this.uah = uah;
    }
}
