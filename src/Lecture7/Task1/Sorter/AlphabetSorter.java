package Lecture7.Task1.Sorter;


import Lecture7.Task1.Finance;

import java.util.Comparator;

public class AlphabetSorter implements Comparator<Finance>{

    @Override
    public int compare(Finance o1, Finance o2) {
        return o1.name.compareTo(o2.name);
    }
}
