package Lecture7.Task1;


import Lecture7.Task1.Interfaces.Exchange;

public class BlackMarket extends Finance implements Exchange {

    private Currency currency;

    public BlackMarket(String name, String adress, double usd, double eur){
        super(name, adress);
        currency = new Currency(usd, eur);
    }

    @Override
    public String getInfo() {
        return super.getInfo();
    }

    @Override
    public double convert(int uah, String curr){
        switch (curr) {
            case "usd":
                return uah / currency.usd;
            case "eur":
                return uah / currency.eur;
            default:
                return 0;
        }
    }
}
