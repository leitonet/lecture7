package Lecture7.Task1;

import Lecture7.Task1.Interfaces.Exchange;
import Lecture7.Task1.Interfaces.Kredit;
import Lecture7.Task1.Interfaces.Sender;
import Lecture7.Task1.Sorter.AlphabetSorter;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Collections;
import java.util.List;

public class Main {
    static List<Finance> array = new LinkedList<Finance>();

    public static void main(String[] args) {
        generateArray();
        printArraySortedByName(array);
        findBestCourse(array, 20_000);
        findBestKredit(array, 50_000);
        findBestSend(array, 1_000);
    }

    public static void printArraySortedByName(List<Finance> array) {
        System.out.println("---by name");
        Collections.sort(array, new AlphabetSorter());
        for (Finance finance : array) {
            System.out.println(finance.getInfo());
        }
        System.out.println("****");
    }

    public static void generateArray() {
        array.add(new Bank("Privat", "Kharkov, Klochkovskaya str., 52", 27.2, 29.5, 2_000_000_000, 0.25, 0.01));
        array.add(new Bank("Pumb", "Kharkov, Klochkovskaya str., 25", 27.5, 30.2, 400_000_000, 0.27, 0.015));
        array.add(new BlackMarket("Viktoriya", "Kharkov, Barabashova, 27", 27.5, 30.08));
        array.add(new BlackMarket("Obmenka", "Kharkov, Barabashova, 22", 27.6, 30.09));
        array.add(new Pawnshop("Tovari za dorogo", "Kiyv, Kharkovskaya, 20", 0.4));
        array.add(new Pawnshop("Lombard", "Kharkov, Saltovskoe Highway, 160", 0.5));
        array.add(new KreditKafe("Bistro Dengi", "Zaporozhie, Tarasa Grigorovycha, 3", 2));
        array.add(new KreditKafe("V dolg", "Kharkov, Solnechnaya, 51", 2.2));
        array.add(new CreditUnion("Union Money", "Kryviy Rig, Gogolya, 109", 0.2));
        array.add(new CreditUnion("Capital", "Kryviy Rig, Pryamaya, 62", 0.5));
        array.add(new MIF("Anatoliy", "Zuzuki vilage, Khromonogih 2", 1955));
        array.add(new MIF("Anatoliy i co.", "Zuzuki vilage, Khromonogih 1", 1855));
        array.add(new Post("Post", "Kiyv, Borshagovskaya, 145", 0.02));
        array.add(new Post("Post", "Kharkov, Solnechnaya, 80", 0.03));
    }

    public static void findBestCourse(List<Finance> array, int uah) {
        Iterator<Finance> iterator = array.iterator();
        double tmp = 0;
        String nameOfConvertBestCourse = "";
        while (iterator.hasNext()) {
            Finance fin = iterator.next();
            try {
                if (fin instanceof Exchange) {
                    double tmp1 = ((Exchange) fin).convert(uah, "usd");
                    if (tmp < tmp1) {
                        tmp = tmp1;
                        nameOfConvertBestCourse = fin.name;
                    }
                }
            } catch (IllegalArgumentException e) {

            }

        }
        for (Finance finance : array) {
            if (finance.name.equals(nameOfConvertBestCourse)) {
                System.out.println("Best course for exchange in - " + finance.getInfo());
            }
        }
        System.out.println("****");
    }

    public static void findBestKredit(List<Finance> array, int uah) {
        Iterator<Finance> iterator = array.iterator();
        double tmp = 0;
        String nameBestOfKredit = "";
        while (iterator.hasNext()) {
            Finance fin = iterator.next();
            try {
                if (fin instanceof Kredit) {
                    double tmp1 = ((Kredit) fin).giveKredit(uah);
                    if (tmp < tmp1) {
                        tmp = tmp1;
                        nameBestOfKredit = fin.name;
                    }
                }
            } catch (IllegalArgumentException e) {
            }
        }
        for (Finance finance : array) {
            if (finance.name.equals(nameBestOfKredit)) {
                System.out.println("Best company for credit - " + finance.getInfo());
            }
        }
        System.out.println("****");
    }

    public static void findBestSend(List<Finance> array, double uah) {
        Iterator<Finance> iterator = array.iterator();
        double tmp = 0;
        String nameBestOfSend = "";
        while (iterator.hasNext()) {
            Finance fin = iterator.next();
            try {
                if (fin instanceof Sender) {
                    double tmp1 = ((Sender) fin).send(uah);
                    if (tmp < tmp1) {
                        tmp = tmp1;
                        nameBestOfSend = fin.name;
                    }
                }
            } catch (IllegalArgumentException e) {
            }
        }
        for (Finance finance : array) {
            if (finance.name.equals(nameBestOfSend)) {
                System.out.println("Best company for send money - " + finance.getInfo());
            }
        }
        System.out.println("****");
    }
}

