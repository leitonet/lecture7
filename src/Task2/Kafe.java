package Task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class Kafe {
    public static Map<String, Double> menu = new HashMap<>();

    public static void main(String[] args) throws IOException {
        generateMenu();
        printMenu(menu);
        System.out.println("*****");
        printCheck(menu, getOrder());
    }

    public static void generateMenu(){
        menu.put("soup", 2.2);
        menu.put("burger", 1.5);
        menu.put("pizza", 3.1);
        menu.put("coffee", 1.0);
        menu.put("tea", 0.8);
        menu.put("steak", 5.0);
        menu.put("potato-free", 1.8);
    }

    public static void printMenu(Map<String, Double> menu){
        Iterator<Map.Entry<String, Double>> iterator = menu.entrySet().iterator();
        System.out.println("Menu: ");
        while (iterator.hasNext()){
            String [] dish = iterator.next().toString().split("=");
            System.out.println("Dish -- " + dish[0] + " *** Price -- " + dish[1] + " $");
        }
    }

    public static String getOrder() throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, enter the name of the dish or several dishes present in the menu with a space: ");
        String dish = reader.readLine();
        return dish;
    }

    public static void printCheck(Map<String, Double> menu, String nameOfDish){
        System.out.println("Your check: ------------------");
        String [] array = nameOfDish.split(" ");
        double totalPrice = 0.0;
        int count;
        HashSet <String> countOfUniqueDish = new HashSet<>();

        for (String dish : array) {
            count = 0;
            for (String match : array) {
                if(match.equals(dish)){
                    count ++;
                }
            }
            countOfUniqueDish.add(dish + " " + count);
        }
        for (String dish : countOfUniqueDish) {
            String [] tmp = dish.split(" ");
            System.out.println("Dish -- " + tmp[0] + " *** Count -- " + tmp[1] + " *** Price -- " + menu.get(tmp[0]) + " $");
            totalPrice += (menu.get(tmp[0])* Integer.parseInt(tmp[1]));
        }

        System.out.println("------------------------------");
        System.out.println("Total price -- " + totalPrice + " $");
    }
}
